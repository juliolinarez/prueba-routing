# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

puts "SEED: Cities"
puts ("="*100)

start = Time.now
cities = [
    {:id => 1, :name => "Cerro Navia" },
    {:id => 2, :name => "Cerrillos" },
    {:id => 3, :name => "Conchalí" },
    {:id => 4, :name => "El Bosque" },
    {:id => 6, :name => "Estación Central" },
    {:id => 5, :name => "Huechuraba" },
    {:id => 7, :name => "Independencia" },
    {:id => 9, :name => "La Cisterna" },
    {:id => 8, :name => "La Florida" },
    {:id => 10, :name => "La Granja" },
    {:id => 12, :name => "La Pintana" },
    {:id => 11, :name => "La Reina" },
    {:id => 13, :name => "Las Condes" },
    {:id => 14, :name => "Lo Barnechea" },
    {:id => 16, :name => "Lo Espejo" },
    {:id => 15, :name => "Lo Prado" },
    {:id => 17, :name => "Macul" },
    {:id => 19, :name => "Maipú" },
    {:id => 18, :name => "Ñuñoa" },
    {:id => 20, :name => "Pedro Aguirre Cerda" },
    {:id => 21, :name => "Peñalolén" },
    {:id => 22, :name => "Providencia" },
    {:id => 24, :name => "Pudahuel" },
    {:id => 23, :name => "Quilicura" },
    {:id => 25, :name => "Quinta Normal" },
    {:id => 26, :name => "Recoleta" },
    {:id => 27, :name => "Renca" },
    {:id => 28, :name => "San Joaquín" },
    {:id => 29, :name => "San Miguel" },
    {:id => 30, :name => "San Ramón" },
    {:id => 31, :name => "Vitacura" },
    {:id => 32, :name => "Santiago" }
]
cities.each do |city|
    City.create!(city)
end

puts "SEED: Unassigned Routes"
routes = [
    {:id => 1, :name => "Cerro Navia" },
    {:id => 2, :name => "Cerrillos" },
    {:id => 3, :name => "Conchalí" },
    {:id => 4, :name => "El Bosque" },
    {:id => 6, :name => "Estación Central" },
    {:id => 5, :name => "Huechuraba" },
    {:id => 7, :name => "Independencia" },
    {:id => 9, :name => "La Cisterna" },
    {:id => 8, :name => "La Florida" },
    {:id => 10, :name => "La Granja" },
    {:id => 12, :name => "La Pintana" },
    {:id => 11, :name => "La Reina" },
    {:id => 13, :name => "Las Condes" },
    {:id => 14, :name => "Lo Barnechea" },
    {:id => 16, :name => "Lo Espejo" },
    {:id => 15, :name => "Lo Prado" },
    {:id => 17, :name => "Macul" },
    {:id => 19, :name => "Maipú" },
    {:id => 18, :name => "Ñuñoa" },
    {:id => 20, :name => "Pedro Aguirre Cerda" },
    {:id => 21, :name => "Peñalolén" },
    {:id => 22, :name => "Providencia" },
    {:id => 24, :name => "Pudahuel" },
    {:id => 23, :name => "Quilicura" },
    {:id => 25, :name => "Quinta Normal" },
    {:id => 26, :name => "Recoleta" },
    {:id => 27, :name => "Renca" },
    {:id => 28, :name => "San Joaquín" },
    {:id => 29, :name => "San Miguel" },
    {:id => 30, :name => "San Ramón" },
    {:id => 31, :name => "Vitacura" },
    {:id => 32, :name => "Santiago" }
]








