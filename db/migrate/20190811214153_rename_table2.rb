class RenameTable2 < ActiveRecord::Migration[5.2]
  def change
    rename_table :route_cities, :cities_routes
  end
end
