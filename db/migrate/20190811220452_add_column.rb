class AddColumn < ActiveRecord::Migration[5.2]
  def change
    add_column :drivers, :stops_amount, :integer
  end
end
