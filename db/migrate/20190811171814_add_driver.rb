class AddDriver < ActiveRecord::Migration[5.2]
  def change
    create_table :drivers do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.references :vehicle, index: true, foreign_key: true
      t.timestamps
    end

    create_table :vehicles do |t|
      t.float :capacity
      t.integer :load_type      
      t.timestamps
    end

    create_table :driver_cities do |t|
      t.references :driver, index: true, foreign_key: true
      t.references :city, index: true, foreign_key: true
      t.timestamps
    end

  end
end
