class RenameTable < ActiveRecord::Migration[5.2]
  def change
    rename_table :driver_cities, :cities_drivers
    remove_column :vehicles, :email
  end
end
