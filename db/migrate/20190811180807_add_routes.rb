class AddRoutes < ActiveRecord::Migration[5.2]
  def change
    create_table :routes do |t|
      t.timestamp :starts_at
      t.timestamp :ends_at
      t.integer :load_type
      t.float :load_sum
      t.integer :stops_amount
      t.references :vehicle, index: true, foreign_key: true
      t.references :driver, index: true, foreign_key: true
      t.timestamps
    end

    create_table :route_cities do |t|
      t.references :route, index: true, foreign_key: true
      t.references :city, index: true, foreign_key: true
      t.timestamps
    end

  end
end
