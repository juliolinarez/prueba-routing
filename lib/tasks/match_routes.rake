desc "match routes"
task :match_routes => [ :environment ] do
  #Rails.logger.level = Logger::DEBUG
  unassigned_routes = Route.where(vehicle_id: nil, driver_id: nil)
  if unassigned_routes.empty?
    puts "No hay rutas sin asignar conductor"
  else
    drivers = Driver.all
    if drivers.empty?
      puts "No hay conductores"
    else
      unassigned_routes.each do |_route|
        drivers.each do |driver|
          if driver.stops_amount.blank?
            # si el conductor no tiene limite de paradas
            stops_valid = true
          else
            stops_valid = _route.stops_amount <= driver.stops_amount
          end
          puts stops_valid
          if stops_valid && know_route_driver(driver, _route)
            if check_overlap_time(driver, _route)
              next
            else  
              driver_vehicle = check_vehicle(driver, _route)
              if driver_vehicle.nil?
                # no hubo un vehiculo apto para esta ruta
                next
              else
                _route.vehicle_id = driver_vehicle.id
                _route.driver_id = driver.id
                _route.save!
                print "vehicle_id: #{driver_vehicle.id} , driver_id: #{driver.id} "
                print ", route_id: #{_route.id} + starts_at: #{_route.starts_at} , ends_at: #{ _route.ends_at} \n"
              end
            end
          else
            next
          end
        end
      end
    end
  end
end

def check_vehicle(driver, _route)
  if driver.vehicle_id.blank?
    used_vehicles_id = Driver.where.not(vehicle_id: nil).map {|d| d.vehicle_id}

    vehicle = Vehicle.where.not(id: used_vehicles_id).detect { |v|
      v.load_type == _route.load_type && v.capacity >= _route.load_sum
    }
    #_route.vehicle_id = vehicle.id
    # TODO : Verficiar la disponibilidad del vehiculo en funcion del tiempo
    #y no solo del tiempo del conductor
    return vehicle
  else
    driver_vehicle = driver.vehicle
    if driver_vehicle.load_type == _route.load_type && driver_vehicle.capacity >= _route.load_sum
      return driver_vehicle
    else
      return nil
    end
  end
end

def check_overlap_time(driver, _route)
  if driver.routes.empty?
    return false
  else
    overlap = driver.routes.select{ |route|
      (_route.starts_at - route.ends_at) * (route.starts_at - _route.ends_at) > 0
    }
    return overlap.present?
  end
end


def know_route_driver(driver, _route)
  if driver.cities.empty?
    # si la lista de cuidades del conductor esta vacia,
    # el conductor conoce todas las comunas
    return true;
  else
    know_cities = driver.cities.map { |dc| dc.id }
    route_required_cities = _route.cities.map { |rc| rc.id}
      # el conductor conoce todas las comunas de la ruta? 
    know_cities_route = (route_required_cities - know_cities).empty?
    return know_cities_route
  end
end

task :clean_routes => [ :environment ] do
  Route.all.update(vehicle_id:nil, driver_id: nil) 
end