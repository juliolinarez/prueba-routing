class Driver < ActiveRecord::Base
    #has_many :cities
    belongs_to :vehicle #, optional: true
    has_and_belongs_to_many :cities
    has_many :routes
end