class Vehicle < ActiveRecord::Base
    enum type: [
        :cooled,
        :standard
    ]
end